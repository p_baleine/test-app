var express = require('express');
var app = express();

app.use(express.logger());
app.use(express.errorHandler());

app.get('/', function(req, res) {
  res.send('Normal path');
});

app.get('/error', function() {
  var e = new Error;
  e.message = 'Catched error 1';
  throw e;
});

app.get('/error2', function(req, res, next) {
  var e = new Error;
  e.message = 'Catched error 2';
  setTimeout(function() {
    next(e);    
  }, 10);
});

app.get('/error3', function(req, res, next) {
  var e = new Error;
  e.message = 'Uncatched error';
  setTimeout(function() {
    throw e;
  }, 10);
});

app.get('/normal', function(req, res, next) {
  res.send('Normal path 2');
});

app.listen(3000);
